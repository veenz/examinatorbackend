/* eslint-disable max-len */

import React from 'react'
import { List, Container, Header, Divider, Button, Segment, Sidebar, Icon, Menu, Image } from 'semantic-ui-react';
export default class App extends React.Component {
  constructor(){
    super();
    this.state = {
      'text': 'ciao;come;stai',
      'title': '',
      'ok': 0,
      'fail': 0,
      'spoiler': false,
      'visible': true,
      'total': 0,
    }
  }
  returnItems = () => {
    return (
      <List celled items={this.state.text.split(";")} />
    )
  }
  componentWillMount(){
    this.total()
    fetch('http://localhost:5000/extract')
    .then(data => data.json())
    .then(data => this.setState({
      'title': data.question,
      'text': data.answer
    }))
    fetch('http://localhost:5000/score/ok').then(data => data.json()).then(data=>this.setState({'ok':data}))
    fetch('http://localhost:5000/score/fail').then(data => data.json()).then(data=>this.setState({'fail':data}))
    setInterval(() => fetch('http://localhost:5000/score/ok').then(data => data.json()).then(data=>this.setState({'ok':data})), 10000)
    setInterval(() => fetch('http://localhost:5000/score/fail').then(data => data.json()).then(data=>this.setState({'fail':data})), 10000)
  }

  extract = () => {
    fetch('http://localhost:5000/extract')
    .then(data => data.json())
    .then(data => this.setState({
      'title': data.question,
      'text': data.answer
    }))
  }
  giuste = () => {
    this.extract()
    this.setState({'spoiler':false})
    fetch('http://localhost:5000/giuste').then(data => data.json()).then(data=>this.setState({'ok':data}))
  }

  sbagliate = () => {
    this.extract()
    this.setState({'spoiler':false})
    fetch('http://localhost:5000/sbagliate').then(data => data.json()).then(data=>this.setState({'fail':data}))
  }
  total = () => {

    fetch('http://localhost:5000/total').then(data => data.json()).then(data => this.setState({'total':data}))
  }
  spoiler = () => {
    if (this.state.spoiler == false){
      return (
        <a onClick={() => this.setState({'spoiler':true})}>Guarda risposta!</a>
      )
    } else {
      return (
        <a onClick={() => this.setState({'spoiler':false})}>Nascondi la risposta</a>
      )
    }
  }
  render(){
    
  return (
    
      <Container text>
      
      <Header as='h2'></Header>
      <Segment>
        <h3>Giuste: {this.state.ok}</h3>
        <h3>Sbagliate: {this.state.fail}</h3>
        <h3> {parseInt(this.state.ok) + parseInt(this.state.fail)} / {this.state.total} domande </h3>
        <h2 style={{'fontSize':'30px'}}>{this.state.title}</h2>
        <Divider hidden />
        {this.spoiler()}
        <div style={{'display': (this.state.spoiler == false) ? 'none':'block'}}>
          <p style={{'fontSize':'22px'}}>{this.returnItems()}</p>
        </div>
        <br/>
        <br />
        <Button color='red' onClick={() => this.sbagliate()} style={{'width':'47%'}}>Sbagliato</Button>
        <Button color='green' onClick={() => this.giuste()} style={{'width':'47%'}}>Giusto</Button>
        <Button color='grey' style={{'width':'94%', 'marginTop':'30px'}} onClick={() => this.extract()} >Skip</Button>
    </Segment>
    
      </Container>
    )}
}

/*
<Sidebar.Pushable as={Segment} >
      <Container text>
      
        <Header as='h2'></Header>
        <Segment>
          <h3>Giuste: {this.state.ok}</h3>
          <h3>Sbagliate: {this.state.fail}</h3>
          <h2 style={{'fontSize':'30px'}}>{this.state.title}</h2>
          <Divider hidden />
          {this.spoiler()}
          <div style={{'display': (this.state.spoiler == false) ? 'none':'block'}}>
            <p style={{'fontSize':'22px'}}>{this.returnItems()}</p>
          </div>
          <br/>
          <br />
          <Button color='red' onClick={() => this.sbagliate()} style={{'width':'47%'}}>Sbagliato</Button>
          <Button color='green' onClick={() => this.giuste()} style={{'width':'47%'}}>Giusto</Button>
          <Button color='grey' style={{'width':'94%', 'marginTop':'30px'}} onClick={() => this.extract()} >Skip</Button>
      </Segment>
      
          <Sidebar
            as={Menu}
            animation='overlay'
            icon='labeled'
            inverted
            onHide={this.handleSidebarHide}
            vertical
            visible={this.state.visible}
            width='thin'
          >
            <Menu.Item as='a'>
              <Icon name='home' />
              Home
            </Menu.Item>
            <Menu.Item as='a'>
              <Icon name='gamepad' />
              Games
            </Menu.Item>
            <Menu.Item as='a'>
              <Icon name='camera' />
              Channels
            </Menu.Item>
          </Sidebar>

        </Container>
        </Sidebar.Pushable>
        */

from flask import Flask, jsonify
from flask_cors import CORS
from functions import *
import redis
app = Flask(__name__)
CORS(app)
data = formatQuestions(getAllLines())
r = redis.Redis(decode_responses=True)
@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/extract')
@app.route('/extract/<repeat>')
def extract(repeat=True):
    e = extractQuestion(data, False)
    return jsonify(e)

@app.route('/total')
def total():
    return jsonify(len(data))

@app.route('/giuste')
def giuste():
    return jsonify(r.incr('giuste'))

@app.route('/sbagliate')
def sbagliate():
    return jsonify(r.incr('errore'))

@app.route('/reset')
def reset():
    r.set('giuste',0)
    r.set('errore',0)
    return 'OK'


@app.route('/score/ok')
def scoreok():
    return jsonify(r.get('giuste'))

@app.route('/score/fail')
def scorefail():
    return jsonify(r.get('errore'))

app.run()
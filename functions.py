import random
import json
def getAllLines():
    with open('questions/module1_3','r') as target:
        data = target.read()
    questions = data.split("\n")
    return questions

def formatQuestions(data):
    questionsAnswers = []
    last = ''
    questionAnswer = {}
    i = 0
    for d in data:
        questionAnswer['id'] = i
        if d[0] == '?' and last != '?':
            questionAnswer['question'] = d[1:]
            
            last = '?'
        if d[0] == '!' and last == '?':
            last = '!'
            questionAnswer['answer'] = d[1:]
        if 'answer' in questionAnswer and 'question' in questionAnswer:
            questionsAnswers.append(questionAnswer)
            questionAnswer = {}
            i += 1
        
    return questionsAnswers

def extractQuestion(questions, repeat=True):
    
    with open('extracted','r') as target:
        extracted = json.load(target)
    #input(extracted)
    if repeat:
        question = random.choice(questions)
        extracted.append(question['id'])
    else:
        while True:
            question = random.choice(questions)
            if question['id'] in extracted:
                continue
            extracted.append(question['id'])
            break
    
    with open('extracted','w+') as target:
        target.write(json.dumps(extracted))

    return question